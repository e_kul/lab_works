﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTac
{
    public partial class Form1 : Form
    {
         MyButton[,] mybtn;
        int counter =0;
        bool isPressed = false;

        public Form1()
        {
            InitializeComponent();
            mybtn = new MyButton[4, 4];
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for(int i=1; i<=3; i++)
            {
                for(int j=1; j<=3; j++)
                {
                    mybtn[i, j] = new MyButton(i, j);
                    mybtn[i, j].Location = new Point(i * 30, j  * 30);
                    mybtn[i, j].Size = new Size(30, 30);
                    mybtn[i, j].Click += new System.EventHandler(buttonClick);
                    this.Controls.Add(mybtn[i, j]);
                }
            }
        }
        private void buttonClick(object sender, EventArgs e)
        {
             MyButton b = sender as MyButton;
             isPressed = true;
             check();
            if(isPressed== true)
            {
                counter++;
            }
            for (int i = 1; i <= 3; i++)
            {
                for (int j = 1; j <= 3; j++)
                {
                    if (counter % 2 != 0)
                       b.Text = "X";
                    else
                       b.Text = "0";
                }
            }

        }
        private void check()
        {
            for(int m=1; m<=3; m++)
            {
                if (mybtn[m, 1].Text == "X" || mybtn[m, 2].Text == "X" || mybtn[m, 3].Text == "X" || mybtn[1, m].Text == "X" || mybtn[2, m].Text == "X" || mybtn[3, m].Text == "X")
                {
                    MessageBox.Show("You WIN! Congrats");
                }
            }
        }
    }
}
