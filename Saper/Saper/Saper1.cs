﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saper
{
    class Saper1
    {
         public int[,] arr;
         public int x, y;
        int n, m, k;
        Random rnd;
        public Saper1(int _n, int _m, int _k)
        {
            n = _n;
            m = _m;
            k = _k;
            arr = new int[n + 2, m + 2];
            rnd = new Random();
            NewRndB();
        }
        public void NewRndB()
    {
         
            for (int i = 0; i < k; i++)
            {
                x = rnd.Next(1, n);
                y = rnd.Next(1, m);
                arr[x, y] = 1;
            }
    }
        public int Move(int x, int y)
        {
            int count=0;
            if(arr[x,y]==1)
            {
                return -1;
            }else{
                for (int i = Math.Abs(x - 1); i <= x + 1; i++)
                {
                    for(int j = Math.Abs(y - 1); j <= y + 1; j++)
                    {
                        if (arr[i, j] == 1)
                        {
                            count++;
                        }
                    }
                }
                return count;
            }
           
        }
    }
}
