﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Saper
{
    public partial class Form1 : Form
    {
        
        public static int n = 5;
        public static int m = 5;
        public static int k = 2;
        public static int winCount = n * m - k;
        public static int pressCounter = 0;
        public int TimerCount = 0;
        Saper1 s;
        MyButton[,] myBut;
        public Form1()
        {
            InitializeComponent();
            s = new Saper1(n, m, k);
            myBut = new MyButton[n,m];
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            for (int i = 0; i < n; i++)
            {
                for(int j =0; j< m; j++)
                {
                    myBut[i,j] = new MyButton(i,j);
                    myBut[i, j].Location = new Point((i+3) * 30, (j+3) * 30);
                    myBut[i, j].Size = new Size(30, 30);
                    myBut[i, j].Click += new System.EventHandler(buttonClick);
                    this.Controls.Add(myBut[i, j]);
                }
            }

        }
        private void buttonClick(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            int p = s.Move(b.x, b.y);
            pressCounter++;
            b.Enabled = false;
            if (p == -1)
            {
                b.Text = "*";
                timer1.Enabled = false;
                MessageBox.Show("GAME OVER");
                restart();
                s.NewRndB();
                TimerCount = 0;
                timer1.Enabled = true;
               
            }
            else b.Text = p.ToString();
            if (win() == true)
            {
                timer1.Enabled = false;
                MessageBox.Show("You WIN! Congrats");
                restart();
                s.NewRndB();
                TimerCount = 0;
                timer1.Enabled = true ;
                
                
            }
        }
        private void restart()
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    myBut[i, j].Enabled=true;
                    myBut[i, j].Text = " ";
                    s.arr[i, j] = 0;
                }
            }
            
            pressCounter = 0;
        }
        private bool win()
        {
            if (winCount == pressCounter)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = TimerCount.ToString();
            TimerCount += 1;
        }

        
    }
}
