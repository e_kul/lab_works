﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Levenshtein
{
    class Program
    {
        static int Levenshtein(string a, string b)
        {
            int[ , ] ar = new int[a.Length + 1, b.Length + 1];
            for (int i = 0; i <= a.Length; i++)
            {
                ar[i, 0] = i;
            }
            for (int j = 0; j <= b.Length; j++)
            {
                ar[0, j] = j;
            }

            for (int i = 1; i <= a.Length; i++)
            {
                for (int j = 1; j <= b.Length; j++)
                {
                    int mn1 = (ar[i - 1, j] + 1 < ar[i, j - 1] ? ar[i - 1, j] + 1 : ar[i, j - 1] + 1);
                    int k = (a[i - 1] != b[j - 1] ? 2 : 0);
                    int mn2 = (ar[i - 1, j - 1] + k < mn1 ? ar[i - 1, j - 1] + k : mn1);
                    ar[i, j] = mn2;
                }
            }
            return ar[a.Length, b.Length];
        }
        static void Main(string[] args)
        {
            StreamReader dic = new StreamReader(@"C:\Users\мм\Desktop\Labs\words.txt");
            StreamReader sr = new StreamReader(@"C:\Users\мм\Desktop\Labs\input.txt");
            StreamWriter sw = new StreamWriter(@"C:\Users\мм\Desktop\Labs\output.txt");
                
            string[] ar = sr.ReadToEnd().Split(' ');
            var words = new List<string>();
            string line;
            while ((line = dic.ReadLine()) != null)
            {
                words.Add(line);
            }
            for (int i = 0; i < ar.Length; i++)
            {
                string s = ar[i].ToLower();
                bool iscorrect = false;
                int mndist = -1;
                string tmp = " ";
                for (int j = 0; j < words.Count; j++)
                {
                    string t = words[j];
                    int dist = Levenshtein(s, t);
                    if (dist == 0)
                    {
                        iscorrect = true;
                    }
                    else if (mndist == -1 || mndist > dist)
                    {
                        mndist = dist;
                        tmp = t;
                    }
                    else if(mndist == dist)
                    {
                    
                    }
                }
                if (iscorrect == true)
                {
                    sw.Write(ar[i] + " ");
                    Console.WriteLine(ar[i] + " : ok!");
                }
                else
                {
                    sw.Write(tmp + " ");
                    Console.WriteLine(ar[i] + " --> " + tmp);
                }
            }
            Console.ReadKey();
            dic.Close();
            sr.Close();
            sw.Close();
           
        }
    }
}
