﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quiz
{
    public partial class Form1 : Form
    {
        int x, y, r, p = 0;
        public Form1()
        {
            InitializeComponent();
            x = this.Width / 2;
            y = this.Height / 2;
            r = 6;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int xx = x - r;
            int yy = y - r;
            Graphics g = this.CreateGraphics();
            Brush MyBrush = new SolidBrush(Color.Red);
            if (r % 4 == 0)
            {
                MyBrush = new SolidBrush(Color.Blue);
            }
            if (r % 4 == 1)
            {
                MyBrush = new SolidBrush(Color.Green);
            }
            if (r % 4 == 2)
            {
                MyBrush = new SolidBrush(Color.Orange);
            }
            if (r % 4 == 3)
            {
                MyBrush = new SolidBrush(Color.Black);
            }
            if (p == 1)
                g.FillEllipse(MyBrush, xx, yy, 2 * r, 2 * r);
            else
                g.FillRectangle(MyBrush, xx, yy, 2 * r, 2 * r);
            r++;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            timer1.Enabled = true;
            p = 1;
            r = 6;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            timer1.Enabled = true;
            p = 2;
            r = 6;
        }

    }
}
