﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace abstract_class
{
    abstract class Figure
    {
        abstract public double Area();
    }
    class Rectangle: Figure
    {
        int a, b;
        public Rectangle(int _a, int _b)
        {
            a=_a;
            b=_b;
        }
        public override double Area()
        {
            return a * b;
        }
        public double Perimetr()
        {
            return 2 * (a + b);
        }

    }
    class Triangle : Figure
    {
        int a, b, c;
        public Triangle(int _a, int _b, int _c)
        {
            a = _a;
            b = _b;
            c = _c;

        }
        public override double Area()
        {
            return Math.Sqrt(((a + b + c) / 2)*(((a + b + c) / 2) - a)*(((a + b + c ) / 2) - b)*(((a + b + c) / 2) - c));
        }
        public double Perimetr()
        {
            return a+b+c;
        }

    }
    class Circle : Figure
    {
        int r;
        public Circle(int _r)
        {
            r = _r;

        }
        public override double Area()
        {
            return Math.PI*Math.Pow(r,2);
        }
        public double Len()
        {
            return 2*Math.PI*r;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rec = new Rectangle(8, 6);
            Console.WriteLine("Area of Rectangle is " + " " + rec.Area());
            Console.WriteLine("Perimetr of Rectangle is " + " " + rec.Perimetr() + "\n");

            Triangle tri = new Triangle(8, 6, 7);
            Console.WriteLine("Area of Triangle is " + " " + tri.Area());
            Console.WriteLine("Perimetr of Triangle is " + " " + tri.Perimetr() + "\n");

            Circle cir = new Circle(4);
            Console.WriteLine("Area of Circle is " + " " + cir.Area());
            Console.WriteLine("Length of Circle is " + " " + cir.Len() + "\n");
            Console.ReadKey();
        }
    }
}
