﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Serialization
{
    public enum Genders : int { male, female };
    public class Student
    {
        public string name;
        public string surname;
        public double gpa;
        [XmlIgnore]
        public Genders gender;
        public Student()
        {

        }
        public Student(string _name, string _surname, double _gpa, Genders _gender)
        {
            name = _name;
            surname = _surname;
            gpa = _gpa;
            gender = _gender;
        }
        public override string ToString()
        {
            return name + " " + surname + " " + gpa + " " + gender;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student a = new Student("Egor", "Egorov", 3.2, Genders.male);
            XmlSerializer ser = new XmlSerializer(typeof(Student));
            StreamWriter sw = new StreamWriter(@"C:\Users\мм\Desktop\Labs\Alltasks\XML\students.xml");
            ser.Serialize(sw, a);
            sw.Close();
            XmlSerializer seri = new XmlSerializer(typeof(Student));
            StreamReader sr = new StreamReader(@"C:\Users\мм\Desktop\Labs\Alltasks\XML\students.xml");
            Student s = new Student();
            s = (Student)seri.Deserialize(sr);
            sr.Close();
            Console.WriteLine(s.name + "`s profile is ready");
            Console.ReadKey();
        }
    }
}