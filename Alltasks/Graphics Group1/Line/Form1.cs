﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Line
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            Pen MyPen = new Pen(Color.Blue, 5);
            g.Clear(Color.White);
            g.DrawLine(MyPen,335,1,335,400); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            Pen MyPen = new Pen(Color.Red, 5);
            g.Clear(Color.White);
            g.DrawLine(MyPen, 105, 1, 105, 400); 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            Pen MyPen = new Pen(Color.Green, 5);
            g.Clear(Color.White);
            g.DrawLine(MyPen, 545, 1, 545, 400); 
        }
    }
}
