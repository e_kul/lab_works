﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace compres
{
     
    class Program
    {
        static void CompressFile(string inFilename, string outFilename)
        {
            FileStream sourceFile = File.Open(inFilename, FileMode.Open, FileAccess.Read);
            FileStream destFile = new FileStream(outFilename, FileMode.Create);
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);
            int theByte = sourceFile.ReadByte();
            while (theByte != -1)
            {
                compStream.WriteByte((byte)theByte);
                theByte = sourceFile.ReadByte();
            }
            sourceFile.Close();
            compStream.Close();
            destFile.Close();
        }

        static void UncompressFile(string inFilename, string outFilename)
        {
            FileStream sourceFile = File.Open(inFilename, FileMode.Open, FileAccess.Read);
            FileStream destFile = new FileStream(outFilename, FileMode.Create);
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Decompress);
            int theByte = compStream.ReadByte();
            while (theByte != -1)
            {
                destFile.WriteByte((byte)theByte);
                theByte = compStream.ReadByte();
            }
            sourceFile.Close();
            compStream.Close();
            destFile.Close();
        }
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(@"C:\Users\мм\Desktop\Labs\Alltasks\compres\input.txt");
            StreamWriter sw = new StreamWriter(@"C:\Users\мм\Desktop\Labs\Alltasks\compres\output.txt");
            string[] s = sr.ReadToEnd().Split();
            sr.Close();
            int n = int.Parse(s[0]);
            int sum = 0;
            int[] ar = new int[n];
            for(int i=0; i<n; i++)
            {
                if(i==n-1)
                {
                    ar[i] = int.Parse(s[s.Length - 1]);
                    sum += ar[i];
                    sw.Write(ar[i] + " ");
                }
                ar[i] = int.Parse(s[i + 1]);
                sum += ar[i];
                Console.WriteLine(ar[i]);
                sw.Write(ar[i] + " ");
            }
            sw.Write(sum);
            sw.Close();
            CompressFile(@"C:\Users\мм\Desktop\Labs\Alltasks\compres\output.txt",@"C:\Users\мм\Desktop\Labs\Alltasks\compres\hz.txt");
            UncompressFile(@"C:\Users\мм\Desktop\Labs\Alltasks\compres\output.txt", @"C:\Users\мм\Desktop\Labs\Alltasks\compres\hz.txt");
        }
    }
}
