﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alltasks
{
    abstract public class Figure
    {

        public class Rectangle
        {
            int side1=0;
            int side2=0;
            public Rectangle(int a, int b)
            {
                side1 = a;
                side2 = b;
            }
            public override int Area()
            {
                return side1*side2;
            }
            public override int Perim()
            {
                return (side1+side2)*2;
            }
            
        }

        public class Circle
        {
            int Rad=0;
             public Circle(int a)
            {
                Rad= a;
            }
            public override double Area1()
            {
                return Rad*3.14;
            }
            public override double Len1()
            {
                return 2*3.14*Rad;
            }
            
        }

        public class Triangle
        {
            int side1=0;
            int side2=0;
            int side3=0;
             public Triangle(int a, int b,int c)
            {
                side1 = a;
                side2 = b;
                side3 = c; 
            }
            public override double Area2()
            {
                return Math.Sqrt(((side1 + side2 + side3) / 2) * (((side1 + side2 + side3) / 2) - side1) * (((side1 + side2 + side3) / 2) - side2) * (((side1 + side2 + side3) / 2) - side3));
            }
            public override int Perim2()
            {
                return side1+side2+side3;
            }
            
        }
    }
    class Program
    {
        static void Main()
        {
            Figure.Rectangle rec = new Figure.Rectangle(5, 6);
            Console.WriteLine("area of rectangle is ", rec.Area());
            Console.WriteLine("perimetr of rectangle is ", rec.Perim());

            Figure.Circle cir = new Figure.Circle(5);
            Console.WriteLine("area of rectangle is ", cir.Area1());
            Console.WriteLine("perimetr of rectangle is ", cir.Len1());

            Figure.Triangle tri = new Figure.Triangle(5, 6, 7);
            Console.WriteLine("area of rectangle is ", tri.Area2());
            Console.WriteLine("perimetr of rectangle is ", tri.Perim2());
        }
    }
}
