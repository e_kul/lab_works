﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace abstClassVehicle
{
    public abstract class Vehicle
    {
        abstract public string properties();
    }
    class Car : Vehicle
    {
        double price, speed;
        int date;
        public Car(double _price, double _speed, int _date)
        {
            price = _price;
            speed = _speed;
            date = _date;
        }
        public override string properties()
        {
            string s = price + "\n" + speed + "\n" + date + "\n";
            return s;
        }

    }
    class Plane : Vehicle
    {
        double price, speed, height;
        int passenger, date;
        public Plane(double _price, double _speed, int _date, double _height, int _passenger)
        {
            price = _price;
            speed = _speed;
            date = _date;
            passenger = _passenger;
            height = _height;
            
        }
        public override string properties()
        {
            string s = price + "\n" + speed + "\n" + date + "\n" + passenger + "\n" + height + "\n";
            return s;
        }
    }
    class Ship : Vehicle
    {
        double price, speed;
        int passenger, date;
        string Port;
        public Ship(double _price, double _speed, int _date, string _Port, int _passenger)
        {
            price = _price;
            speed = _speed;
            date = _date;
            passenger = _passenger;
            Port = _Port;
        }
        public override string properties()
        {
            string s = price + "\n" + speed + "\n" + date + "\n" + passenger + "\n" + Port + "\n";
            return s;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Car MyCar = new Car(1300,200.8,2006);
            Ship MyShip = new Ship(320000, 16, 2008, "st.Marry", 400);
            Plane MyPlane = new Plane(4000000,386,2007,192.8,780);
            Console.WriteLine("Price,Speed,Manufacturing date of the car are :" + "\n" + MyCar.properties() + "\n");
            Console.WriteLine("Price,Speed,Manufacturing date,Capacity and Name of The Port of the ship are :" + "\n" + MyShip.properties() + "\n");
            Console.WriteLine("Price,Speed,Manufacturing date,Capacity and Flight height of the plane are :" + "\n" + MyPlane.properties() + "\n");
            Console.ReadKey();
        }
    }
}
