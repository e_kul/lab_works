﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake_new
{
    public partial class Form1 : Form
    {
        Graphics g;
        int x = 50, y = 50;
        int dir;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen p = new Pen(Color.Green, 6);
            g.DrawEllipse(p, 50, 50, 5, 5);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (dir == 0)
            {
                y += 10;
            }
            else if (dir == 1)
            {
                x += 10;
            }else if(dir == 2)
            {
                x -= 10;
            }else if(dir == 3)
            {
                y -= 10;
            }
            Draw();
        }
        private void Draw()
        {
            g.Clear(Color.White);
            g = this.CreateGraphics();
            Pen p = new Pen(Color.Green, 5);
            g.DrawEllipse(p, x, y, 5, 5);
        }
        private void onKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                dir = 1;
            }
            else if (e.KeyCode == Keys.Up)
            {
                dir = 3;
            }
            else if (e.KeyCode == Keys.Left)
            {
                dir = 2;
            }
            else if (e.KeyCode == Keys.Down)
            {
                dir = 0;
            }
        }
    }
}
