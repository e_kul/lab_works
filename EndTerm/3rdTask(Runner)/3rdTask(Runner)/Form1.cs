﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3rdTask_Runner_
{
    public partial class Form1 : Form
    {
        int counter=0;
        Graphics g;
        public int x=10,y=40;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

           
        }
        private void Draw()
        {
            g = this.CreateGraphics();
            g.Clear(Color.White);
            Pen p = new Pen(Color.Red, 6);
            g.DrawEllipse(p, x, y, 10, 10);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            x += 10;
           
            Draw();
        }
        private void onKeyUp(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Space)
            {
                y = Height - 40;
                counter++;
            }

        }
    }
}
