﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2ndTask
{
    public partial class Form1 : Form
    {
        MyButton[,] btns = new MyButton[8, 8];
        public Form1()
        {
            InitializeComponent();
            for(int i=0; i< 8; i++)
            {
                for(int j=0; j<8; j++)
                {
                    btns[i, j] = new MyButton(i, j);
                    btns[i, j].Size = new Size(30, 30);
                    btns[i, j].Location = new Point(i * 30, j * 30);
                    btns[i, j].BackColor = Color.White;
                    btns[i, j].Click += new EventHandler(button_click);
                    this.Controls.Add(btns[i, j]);
                }
            }

        }
        private void button_click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton; 
            for(int i=0; i<8; i++)
            {

                //sama kletka
                if (btns[b.x, b.y].BackColor == Color.Red)
                {
                    btns[b.x, b.y].BackColor = Color.Red;
                }
                else
                {
                    btns[b.x, b.y].BackColor = Color.Red;
                }
                //vertical
                if (btns[b.x, i].BackColor == Color.Red)
                {
                    btns[b.x, i].BackColor = Color.White;
                }
                else
                {
                    btns[b.x, i].BackColor = Color.Red;
                }
                //horizontal
                if (btns[i, b.y].BackColor == Color.Red)
                {
                    btns[i, b.y].BackColor = Color.White;
                }
                else
                {
                    btns[i, b.y].BackColor = Color.Red;
                }
                
            }
        }
    }
}
