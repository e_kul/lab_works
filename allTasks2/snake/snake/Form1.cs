﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake
{
    public partial class Form1 : Form
    {
        int x, y, r, p = 0;
        public Form1()
        {
            InitializeComponent();
            x = 100;
            y = 100;
            r = 6;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int xx = x - r;
            int yy = y - r;
            Graphics g = this.CreateGraphics();
            Brush MyBrush = new SolidBrush(Color.Red);
            
            if (p == 1)
            {
                g.Clear(Color.White);
                g.FillEllipse(MyBrush, xx, yy, 2 * r, 2 * r);
                x++;
                if(x > 300) 
                {
                    y++;
                    x--;
                } 
            }
                
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            timer1.Enabled = true;
            p = 1;
            r = 6;
        }
    }
}
