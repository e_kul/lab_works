﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake1
{
    class Food
    {
         public int[,] arr;
         public int x, y;
        int n, m;
        public Food(int _n, int _m)
        {
            n = _n;
            m = _m;
            arr = new int[n, m];
            Random rnd = new Random();
            for (int i = 0; i < n * m; i++)
            {
                x = rnd.Next(1, n);
                y = rnd.Next(1, m);
                arr[x, y] = 1;
            }
            
        }
    }
}
