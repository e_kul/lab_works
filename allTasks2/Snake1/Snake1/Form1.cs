﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake1
{
    public partial class Form1 : Form
    {
        int x = 100, y = 100, dir = 0;
        Graphics g;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            g = this.CreateGraphics();
            Pen Myp = new Pen(Color.Red,10);
            g.DrawRectangle(Myp, 800, 800, 10, 10);
            Draw1();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (dir == 0)
            {
                y += 10;
            }
            else if (dir == 3)
            {
                x += 10;
            }
            else if (dir == 1)
            {
                x -= 10;
            }
            else if (dir == 2)
            {
                y -= 10;
            }
            if (x > this.Width)
                x = 0;
            if (x < 0)
                x = this.Width;

            if (y > this.Height)
                y = 0;
            if (y < 0)
                y = this.Height;
            Draw();
        }
        private void Draw()
        {
            g.Clear(Color.White);
            g = this.CreateGraphics();
            Pen p = new Pen(Color.Red, 6);
            g.DrawEllipse(p, x, y, 5, 5);
        }
        private void Draw1()
        {
            
            g = this.CreateGraphics();
            Brush MyBrush = new SolidBrush(Color.Brown);
            g.FillRectangle(MyBrush, x, y, this.Width, this.Height);
        }

        private void onKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                dir = 3;
            }
            else if (e.KeyCode == Keys.Up)
            {
                dir = 2;
            }
            else if (e.KeyCode == Keys.Left)
            {
                dir = 1;
            }
            else if (e.KeyCode == Keys.Down)
            {
                dir = 0;
            }
        }
    }
}
